import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  public submittedLogin: boolean = false;
  public submittedReg: boolean = false;
  public loginForm: FormGroup = null;
  public registerForm: FormGroup = null;
  public step: number = 1;

  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public auth: AuthService,
    public toastr: ToastrService,
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  ngOnInit() {}

  get formDataLogin() {
    return this.loginForm.controls;
  }

  get formDataReg() {
    return this.registerForm.controls;
  }

  stepChange(step: number) {
    this.step = step;

    // switch (step) {
    //   case 2:
    //     break;
    // }
  }

  login() {
    this.submittedLogin = true;

    if (this.loginForm.invalid) {
      return false;
    } else {
      this.auth.login(this.loginForm.value).subscribe(
        (res) => {
          let token = res['token'];
          console.log(res);

          if (token) {
            this.toastr.success('Logged in successfully', 'Success');
            this.router.navigate(['/dashboard']);
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  register() {
    this.submittedReg = true;

    if (this.registerForm.invalid) {
      return false;
    } else {
      this.auth.register(this.registerForm.value).subscribe(
        (res) => {
          if (res && res['token']) {
            this.toastr.success('User registered successfully', 'Success');
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }
}
