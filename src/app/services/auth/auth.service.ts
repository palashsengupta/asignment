import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userSubject: BehaviorSubject<Object> = new BehaviorSubject(null);
  constructor(public http: HttpClient) { }

  get userData() {
    return this.userSubject.value;
  }

  login(authData: Object): Observable<any> {
    return this.http.post(`${environment.api}/api/login`, authData)
      .pipe(map((response) => {
        if (response && response['token']) {
          localStorage.setItem('token', response['token']);
          this.userSubject.next(response);

          return response;
        }
      })
    );
  }
  
  register(authData: Object) {
    return this.http.post(`${environment.api}/api/register`, authData);
  }

  logout() {
    localStorage.removeItem('token');
    this.userSubject.next(null);
  }
}
